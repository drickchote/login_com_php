<?php
session_start();

/* unset está tirando a declaração da $_SESSION, se o usuário
tentar acessar localhost:8000/home.php diretamente, ele vai cair no if
do autentica_rota.php e ser redirecionado para a index*/

unset($_SESSION['usuario']);

/* Redireciona o usuário para a index*/
header('Location: index.php');

?>