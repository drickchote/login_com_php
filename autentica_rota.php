<?php

/* Todas as páginas que precisarem de autenticação de rota 
só precisa dar require('autentica_rota.php'), nesse código
só usamos isso no home.php, mas poderiamos usar em várias páginas*/

/* Se o usuário não foi autenticado, volte para a / (index)*/

if(!isset($_SESSION['usuario'])){
     header('Location: /index');
}

/* Que tal fazer aparecer um erro quando o usuário 
tentar acessar diretamente? Uma rota, ou seja, se o if acima der true*/
// Dica: usar session ou passar por parametro /index?parametro=

?>