<?php
/*se for usar $_SESSION, isso é necessário */
session_start(); 
$usuario = $_POST['login'];
$senha = $_POST['senha'];

/* usuarios permitidos */
$usuarios = [
    array('login'=>'marcos', 'senha'=>'123'),
    array('login'=>'diego', 'senha'=>'olamundo'),
    array('login'=>'lucas', 'senha'=>'1234'),
    array('login'=>'pedro', 'senha'=>'123'),
];

$autenticado = false;

/* *Percorre o array $usuarios, veja a versão do for normal abaixo*/
foreach($usuarios as $variavel){
    if($usuario == $variavel['login'] && $senha == $variavel['senha']){
        $autenticado = true;
        break;
    }
}
/* Esse for faz o mesmo que o de cima, compare os dois!*/
// for($i=0;i<sizeof($usuarios); $i++){
//     if($usuario == $usuarios[$i]['login'] && $senha == $usuarios[$i]['senha']){
//         $autenticado = true;
//         break;
//     }
// }

/* Se não estiver autenticado, volte para a index */
if($autenticado){
    /* A declaração da SESSION ocorre aqui */
    $_SESSION['usuario'] = $usuario;
    header('Location: home.php');
} else {
    $_SESSION['senhaerradatio']='sopradeclarar';
    /*Perceba que eu tirei o parâmetro "erro" da url, olhe lá na index*/
    header('Location: /');

    /*Na capacitação foi feito assim, retire o comentário abaixo
    e da index se quiser mudar a forma de verificar o erro*/
    // header('Location: index.php?erro=')// opcao2;
}

?>